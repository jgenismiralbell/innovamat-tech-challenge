# Innovamat Tech Challenge
## Element Description
The project is structured in 3 domains marked by encapsulating namespaces.
- **Framework**: reusable code for other features.
- **Feature**: the concrete feature logic.
    - **Core**: with the gameplay logic.
    - **UI**: with the UI logic.

The decision to group the game logic this way, was motivated by the necessity to make large projects
more understandable and also to mark clearly what code was implemented with the intention to be reused later.

### Framework
In the framework we have 3 classes:
- **State & StateMachine**: generalized classes to apply the state pattern easily.
- **TextFileParser**: extension class to add parse functionality to the Unity Engine TextAsset class.

### Feature
#### Core
This contains the core gameplay logic. Here there are also 4 classes:
- **Test**: this class is conceptualized as a data payload to be created every time the player starts a new test of
the feature. In addition the test class contains some logic for the resolution of the test.
- **Solution**: in the same way as the Test class, a data payload to make easier the presentation of information 
to the player.
- **TestProvider**: this class extends the UnityEngine ScriptableObject class. The reason for using ScriptableObjects
is that the service of providing test can be easily assigned throgh the Inspector and shared between scenes of the 
game without need for any manager for the test generation. Also ScriptableObjects enables a layer of configuration
to the developer directly from the project.
- **ScoreSystem**: another ScriptableObject in order too keep trak of the player's score and notify to observers the
changes in the score.

#### UI
This contains the UI logic. Here there are  4 main classes, but the list can be more extense if we include all 
the state classes of the various state machines.
- **TestPresenter**: the UI manager of the feature.
- **SolutionPresenter**: the UI elements that the player interacts with.
- **SolutionPresenterPool**: the pool used to ensure that the setting of the **TestPresenter** dose not generate too much
memory load and garbage by instanciating and destroying **SolutionPresenter**'s;
- **ScorePresenter**: the HUD element that presents the score of failed and passed tests.

## Design Decisions
I usually implement states of the state machine as internal classes of the same state machine class, but with more complex
states may it would be better to place them in its own file, for readability’s sake. States are designed to be constant
pieces of logic that modify the managed object so that why they are static object in the state machine class.
I've tried to keep the classes decoupled as possible, and communicate classes through actions. The number of solutions can 
be configured and set dynamically. Some edge cases are taken into account, it would work from 3 to the max number of solutions
in the config file.

## Comments
The git history might be a bit of a mess because I should have added a git ignore at the begining.




