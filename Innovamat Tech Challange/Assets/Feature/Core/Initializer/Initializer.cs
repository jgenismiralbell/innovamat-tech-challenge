﻿using Feature.UI;
using UnityEngine;

namespace Feature.Core
{
    public class Initializer : MonoBehaviour
    {
        [SerializeField]
        private ScoreSystem _scoreSystem;
        [SerializeField]
        private ChallengeProvider _challengeProvider;
        [SerializeField]
        private ChallengeView _challengeView;
        [SerializeField]
        private ScoreView _scoreView;

        private Challenge _currentChallenge;

        private void Start()
        {
            _challengeView.Initialize();
            _scoreView.Initialize(_scoreSystem);
            _scoreSystem.Initialize();
            SetChallenge(_challengeProvider.GetChallenge());
        }

        private void SetChallenge(Challenge challenge)
        {
            _currentChallenge = challenge;
            _challengeView.Present(challenge);
            _challengeView.OnChallangeEnded += InitializeNextChallange;
            _scoreSystem.Subscrive(challenge);
        }

        private void RemoveChallege(Challenge challenge)
        {
            _challengeView.OnChallangeEnded -= InitializeNextChallange;
            _scoreSystem.Unsubscrive(challenge);
        }

        private void InitializeNextChallange()
        {
            RemoveChallege(_currentChallenge);
            SetChallenge(_challengeProvider.GetChallenge());
        }
    }
}
