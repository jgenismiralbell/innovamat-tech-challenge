﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Feature.Core
{
    /// <summary>
    /// Centralized system for score registry of the challanges progression.
    /// </summary>
    [CreateAssetMenu(fileName = "ScoreSystem", menuName = "Innovamat Tech Challange/Score System", order = 0)]
    public class ScoreSystem : ScriptableObject
    {
        public Action<int> OnPassedScoreValueChange;
        public Action<int> OnFailedScoreValueChange;

        private int _passedChallenges;
        private int _failedChallenges;

        private Challenge _currentChallenge;

        public void Initialize()
        {
            _passedChallenges = 0;
            _failedChallenges = 0;
            OnPassedScoreValueChange?.Invoke(this._passedChallenges);
            OnFailedScoreValueChange?.Invoke(this._failedChallenges);

        }

        public void Subscrive(Challenge challenge)
        {
            if(_currentChallenge != null)
            {
                _currentChallenge.OnChallengeCompleted -= RefershScore;
            }
            _currentChallenge = challenge;
            _currentChallenge.OnChallengeCompleted += RefershScore;
        }

        public void Unsubscrive(Challenge challenge)
        {
            if(_currentChallenge != null)
            {
                _currentChallenge.OnChallengeCompleted -= RefershScore;
            }
        }

        private void RefershScore(Challenge.OptionSumission sumission)
        {
            if (sumission.IsCorrect)
            {
                _passedChallenges++;
                OnPassedScoreValueChange?.Invoke(this._passedChallenges);
            }
            else
            {
                _failedChallenges++;
                OnFailedScoreValueChange?.Invoke(this._failedChallenges);
            }
        }
    }
}

