using System;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;

namespace Feature.Core
{
    public partial class Challenge
    {
        /// <summary>
        /// Info container for all the options in the challange. It contains the value pair that will be used as
        /// challenge options
        /// </summary>
        [System.Serializable]
        public class Option
        {
            public Challenge Challenge;
            [SerializeField]
            private string _name;
            public string Name => this._name;
            [SerializeField]
            private int _value;
            public int Value => this._value;
            public bool IsCorrect => Challenge.IsCorrect(this);
            public Option(int value, string name)
            {
                _name = name;
                _value = value;
            }

            public void NotifyPick()
            {
                Challenge.SummitOption(this);
            }
        }
    }
}