﻿using System;
using System.Linq;
using System.Collections.Generic;
using Feature.UI;
using UnityEngine;

namespace Feature.Core
{
    /// <summary>
    /// Main bussines logic class. The challange contains the logic for the test proposed to the player
    /// </summary>
    public partial class Challenge
    {
        public Action<OptionSumission> OnChallengeCompleted;
        public Action<OptionSumission> OnOptionSumited;
        private readonly List<OptionSumission> _sumissions = new List<OptionSumission>();
        private readonly Option _correctOption;
        public Option CorrectOption => _correctOption;
        private readonly List<Option> _incorrectOptions;
        public List<Option> AllOptions
        {
            get
            {
                List<Option> value = new List<Option>(_incorrectOptions);
                value.Add(_correctOption);
                System.Random random = new System.Random();
                value = new List<Option>(value.OrderBy(s => random.Next()));        
                return value;
            }
        }

        public Challenge(Option correctOption, List<Option> incorrectOptions)
        {
            _correctOption = correctOption;
            _correctOption.Challenge = this;
            _incorrectOptions = incorrectOptions;
            foreach (Option option in incorrectOptions)
            {
                option.Challenge = this;
            }
        }

        private bool IsCorrect(Option option) => option.Value == this._correctOption.Value;
        
        public void SummitOption(Option option)
        {
            bool isCorrect = IsCorrect(option);
            bool challangeMustEnd = isCorrect || (_sumissions.Count == 1);
            OptionSumission sumission = new OptionSumission(option, isCorrect, challangeMustEnd); 
            _sumissions.Add(sumission);
            if(challangeMustEnd)
            {
                OnChallengeCompleted?.Invoke(sumission);
            }
            else
            {
                OnOptionSumited.Invoke(sumission);
            }
        }

        /// <summary>
        /// Payload for the management of player sumissions on the challange.
        /// </summary>
        public class OptionSumission
        {
            private readonly Option _option;
            public Option Option => _option;
            private readonly bool _isCorrect;
            public bool IsCorrect => _isCorrect;
            private readonly bool _challangeMustEnd;
            public bool ChallangeMustEnd => _challangeMustEnd;

            public OptionSumission(Option option, bool isCorrect, bool challangeMustEnd)
            {
                _option = option;
                _isCorrect = isCorrect;
                _challangeMustEnd = challangeMustEnd;
            }
        }
    }
}