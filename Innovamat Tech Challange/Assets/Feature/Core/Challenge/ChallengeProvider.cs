﻿using System.Collections.Generic;
using UnityEngine;
using Framework;
using UnityEngine.Serialization;

namespace Feature.Core
{
    /// <summary>
    /// Configuratable scriptable objects that provide the challenges
    /// </summary>
    [CreateAssetMenu(fileName = "Challenge Provider", menuName = "Innovamat Tech Challange/Challenge Provider", order = 0)]
    public class ChallengeProvider : ScriptableObject 
    {
        private const string OPTION_OVERSHOOT_WARNING = "The option size ({0}) is set at a hiegher number that the number of possible option ({1})";
        public int OptionSize;
        public TextAsset OptionConfig;
        private List<Challenge.Option> _possibleOptions;

        private List<Challenge.Option> PossibleOptions
        {
            get
            {
                if(this._possibleOptions == null || this._possibleOptions.Count == 0)
                {
                    this._possibleOptions = new List<Challenge.Option>();
                    string[][] optionConfigs = this.OptionConfig.Parse(ParseFormat.CSV);
                    foreach (string[] config in optionConfigs)
                    {
                        Challenge.Option option = new Challenge.Option(int.Parse(config[0]), config[1]);
                        this._possibleOptions.Add(option);
                    }
                }
                return this._possibleOptions;
            }
        }

        public Challenge GetChallenge()
        {
            if(this.PossibleOptions.Count == 0)
            {
                #if UNITY_EDITOR
                    Debug.LogError(string.Format(OPTION_OVERSHOOT_WARNING, this.OptionSize, this.PossibleOptions.Count));
                #endif
                return null;
            }

            if(this.OptionSize > this.PossibleOptions.Count)
            {
            #if UNITY_EDITOR
                Debug.LogWarning(string.Format(OPTION_OVERSHOOT_WARNING, this.OptionSize, this.PossibleOptions.Count));
            #endif
                Challenge.Option correctOption = this.GetRandomCorrectOption();
                return new Challenge(correctOption, this.GetRandomIncorrectOptions(correctOption, this.PossibleOptions.Count));
            }
            else
            {
                Challenge.Option correctOption = this.GetRandomCorrectOption();
                return new Challenge(correctOption, this.GetRandomIncorrectOptions(correctOption, this.OptionSize));
            }
        }
        
        private Challenge.Option GetRandomCorrectOption() => this.PossibleOptions[Random.Range(0, this.PossibleOptions.Count)];

        private List<Challenge.Option> GetRandomIncorrectOptions(Challenge.Option correctOption, int optionSize)
        {
            List<Challenge.Option> incorrectOptions = new List<Challenge.Option>();
            while (incorrectOptions.Count < optionSize - 1)
            {
                Challenge.Option candidate = this.PossibleOptions[Random.Range(0, this.PossibleOptions.Count)];
                if(candidate != correctOption && !incorrectOptions.Contains(candidate)) incorrectOptions.Add(candidate);
            }
            return incorrectOptions;
        }
    }
}
