﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Feature.Core;

namespace Feature.UI
{
    /// <summary>
    /// UI element to display the score of the user sended throw the ScoreSystem
    /// </summary>
    public class ScoreView : MonoBehaviour
    {
        private const string PASSED_SCORE_MESSAGE = "Aciertos: {0}";
        private const string FAILED_SCORE_MESSAGE = "Fallos: {0}";
        private ScoreSystem _scoreSystem;
        [SerializeField]
        private Text _passedTestsDisplay;
        [SerializeField]
        private Text _failedTestsDisplay;

        public void Initialize(ScoreSystem scoreSystem)
        {
            _scoreSystem = scoreSystem;
            _scoreSystem.OnPassedScoreValueChange += this.RefreshPassedScore;
            _scoreSystem.OnFailedScoreValueChange += this.RefreshFailedScore;
        }

        private void RefreshPassedScore(int passedTestAmount)
        {
            this._passedTestsDisplay.text = string.Format(PASSED_SCORE_MESSAGE, passedTestAmount);
        }
        private void RefreshFailedScore(int failedTestAmount)
        {
            this._failedTestsDisplay.text = string.Format(FAILED_SCORE_MESSAGE, failedTestAmount);
        }
    }    
}
