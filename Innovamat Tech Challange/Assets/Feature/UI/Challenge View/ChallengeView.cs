﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Feature.Core;
using Framework;
using UnityEngine.Serialization;

namespace Feature.UI
{
    /// <summary>
    /// UI element to display the challange
    /// </summary>
    public class ChallengeView : MonoBehaviour
    {
        public Action OnChallangeEnded;
        private ChallengeViewStateMachine _stateMachine;
        [HideInInspector]
        public Challenge CurrentChallenge;
        [HideInInspector]
        public readonly List<OptionView> OptionViews = new List<OptionView>();
        public float AnimationDuration = 2.0f;
        [SerializeField]
        private OptionView _optioViewOriginal;

        [SerializeField]
        private Pool<OptionView> _optionViewPool;   
        [FormerlySerializedAs("_correctOptionDisplay")]
        public Text CorrectOptionDisplay;
        [SerializeField]
        private RectTransform _optionPresenterContainer;

        public void Initialize()
        {
            _optionViewPool = new Pool<OptionView>(_optioViewOriginal);
            _stateMachine = new ChallengeViewStateMachine(this);
        }

        private void Update() 
        {
            _stateMachine.Update();
        }

        public void Present(Challenge challenge)
        {
            Unsuscrive(CurrentChallenge);
            CurrentChallenge = challenge;
            Subscrive(CurrentChallenge);
            CorrectOptionDisplay.text = this.CurrentChallenge.CorrectOption.Name;
            for (int n = 0; n < this.OptionViews.Count; n++)
            {
                OptionViews[n].Dispose();
            }
            OptionViews.Clear();
            OptionView view = null;
            foreach (Challenge.Option option in this.CurrentChallenge.AllOptions)
            {
                view = _optionViewPool.Get(this._optionPresenterContainer);
                view.gameObject.SetActive(false);
                view.Present(option);
                OptionViews.Add(view);
            }
            _stateMachine.SetState(ChallengeViewStateMachine.ShowingCorrectOption);
        }

        private void Subscrive(Challenge challenge)
        {
            if(challenge == null) return;
            challenge.OnChallengeCompleted += EndChallenge;
        }

        private void Unsuscrive(Challenge challenge)
        {
            if(challenge == null) return;
            challenge.OnChallengeCompleted -= EndChallenge;
        }

        private void EndChallenge(Challenge.OptionSumission sumission)
        {
            _stateMachine.SetState((sumission.IsCorrect) ? ChallengeViewStateMachine.ChallangePassed : ChallengeViewStateMachine.ChallangeFailed);
        }
    }    
}
