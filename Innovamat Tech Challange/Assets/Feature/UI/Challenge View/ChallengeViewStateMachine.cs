﻿using System.Collections;
using System.Collections.Generic;
using Feature.Core;
using UnityEngine;
using Framework;

namespace Feature.UI
{
    /// <summary>
    /// State machine for the UI element ChallengeView
    /// </summary>
    public class ChallengeViewStateMachine : StateMachine<ChallengeView>
    {
        private static State<ChallengeView> _idle;
        public static State<ChallengeView> ShowingCorrectOption;
        private static State<ChallengeView> _presentingCorrectOption;
        private static State<ChallengeView> _hidingCorrectQuestion;
        private static State<ChallengeView> _showingAllOptions;
        private static State<ChallengeView> _waitingForPlayerInput;
        public static State<ChallengeView> ChallangePassed;
        public static State<ChallengeView> ChallangeFailed;

        public ChallengeViewStateMachine(ChallengeView managedObject) : base(managedObject)
        {
            _idle = new Idle(this);
            ShowingCorrectOption = new ShowingCorrectSolution(this);
            _presentingCorrectOption = new PresentingCorrectSolution(this);
            _hidingCorrectQuestion = new HidingCorrectSolution(this);
            _showingAllOptions = new ShowingAllSolution(this);
            _waitingForPlayerInput = new WaitingForPlayerInput(this);
            ChallangePassed = new TestPassed(this);
            ChallangeFailed = new TestFailed(this);
            CurrentState = _idle;
        }

        public void SetState(State<ChallengeView> state)
        {
            CurrentState.Exit(state);
        }
        
        public void Update()
        {
            CurrentState.Execute();
        }

        private class Idle : State<ChallengeView>
        {
            public Idle(StateMachine<ChallengeView> stateMachine) : base(stateMachine) { }
        }

        private class ShowingCorrectSolution : State<ChallengeView>
        {
            private float _timer;

            protected override void Enter(StateMachine<ChallengeView> machine)
            {
                base.Enter(machine);
                StateMachine.ManagedObject.CorrectOptionDisplay.transform.localScale = Vector3.zero;
                StateMachine.ManagedObject.CorrectOptionDisplay.text = StateMachine.ManagedObject.CurrentChallenge.CorrectOption.Name;
                StateMachine.ManagedObject.CorrectOptionDisplay.gameObject.SetActive(true);
                _timer = 0.0f;
            }

            public override void Execute()
            {
                _timer += Time.deltaTime;
                StateMachine.ManagedObject.CorrectOptionDisplay.transform.localScale = Vector3.Lerp(
                    StateMachine.ManagedObject.CorrectOptionDisplay.transform.localScale,
                    Vector3.one,
                    _timer/StateMachine.ManagedObject.AnimationDuration
                );
                if(_timer >= StateMachine.ManagedObject.AnimationDuration)
                {
                    StateMachine.ManagedObject.CorrectOptionDisplay.transform.localScale = Vector3.one;
                    this.Exit(_presentingCorrectOption);
                }
            }

            public ShowingCorrectSolution(StateMachine<ChallengeView> stateMachine) : base(stateMachine) { }
        }

        private class PresentingCorrectSolution : State<ChallengeView>
        {
            private float _timer;

            protected override void Enter(StateMachine<ChallengeView> machine)
            {
                base.Enter(machine);
                _timer = 0.0f;
            }

            public override void Execute()
            {
                this._timer += Time.deltaTime;
                if(_timer >= StateMachine.ManagedObject.AnimationDuration)
                {
                    this.Exit(_hidingCorrectQuestion);
                }
            }

            public PresentingCorrectSolution(StateMachine<ChallengeView> stateMachine) : base(stateMachine) { }
        }


        private class HidingCorrectSolution: State<ChallengeView>
        {
            private float _timer;

            protected override void Enter(StateMachine<ChallengeView> machine)
            {
                base.Enter(machine);
                _timer = 0.0f;
            }

            public override void Execute()
            {
                _timer += Time.deltaTime;
                StateMachine.ManagedObject.CorrectOptionDisplay.transform.localScale = Vector3.Lerp(
                    StateMachine.ManagedObject.CorrectOptionDisplay.transform.localScale,
                    Vector3.zero,
                    _timer/StateMachine.ManagedObject.AnimationDuration
                );
                if(_timer >= StateMachine.ManagedObject.AnimationDuration)
                {
                    StateMachine.ManagedObject.CorrectOptionDisplay.transform.localScale = Vector3.zero;
                    StateMachine.ManagedObject.CorrectOptionDisplay.gameObject.SetActive(false);
                    this.Exit(_showingAllOptions);
                }
            }

            public HidingCorrectSolution(StateMachine<ChallengeView> stateMachine) : base(stateMachine) { }
        }

        private class ShowingAllSolution : State<ChallengeView>
        {
            protected override void Enter(StateMachine<ChallengeView> machine)
            {
                base.Enter(machine);
                foreach (OptionView presenter in StateMachine.ManagedObject.OptionViews)
                {
                    presenter.SetAsShowing(true);
                }
                this.Exit(_waitingForPlayerInput);
            }

            public ShowingAllSolution(StateMachine<ChallengeView> stateMachine) : base(stateMachine) { }
        }

        private class WaitingForPlayerInput : State<ChallengeView>
        {
            public WaitingForPlayerInput(StateMachine<ChallengeView> stateMachine) : base(stateMachine) { }
        }

        private class TestPassed : State<ChallengeView>
        {
            private float _timer;

            protected override void Enter(StateMachine<ChallengeView> machine)
            {
                base.Enter(machine);
                _timer = 0.0f;
                foreach (OptionView presenter in StateMachine.ManagedObject.OptionViews)
                {
                    presenter.SetAsShowing(false);
                }
            }

            public override void Execute()
            {
                this._timer += Time.deltaTime;
                if(_timer >= StateMachine.ManagedObject.AnimationDuration)
                {
                    StateMachine.ManagedObject.OnChallangeEnded.Invoke();
                    Exit(ShowingCorrectOption);
                }
            }

            public TestPassed(StateMachine<ChallengeView> stateMachine) : base(stateMachine) { }
        }

        private class TestFailed : State<ChallengeView>
        {
            private float _timer;

            protected override void Enter(StateMachine<ChallengeView> machine)
            {
                base.Enter(machine);
                _timer = 0.0f;
                foreach (OptionView presenter in StateMachine.ManagedObject.OptionViews)
                {
                    presenter.SetAsShowing(false);
                }
            }
            
            public override void Execute()
            {
                this._timer += Time.deltaTime;
                if(_timer >= StateMachine.ManagedObject.AnimationDuration)
                {
                    StateMachine.ManagedObject.OnChallangeEnded.Invoke();
                    Exit(ShowingCorrectOption);
                }
            }

            public TestFailed(StateMachine<ChallengeView> stateMachine) : base(stateMachine) { }
        }

    }
}
