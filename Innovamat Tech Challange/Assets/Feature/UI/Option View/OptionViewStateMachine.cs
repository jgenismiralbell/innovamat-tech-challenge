﻿using System.Collections;
using System.Collections.Generic;
using Feature.UI;
using Framework;
using UnityEngine;
namespace Feature.UI
{
    /// <summary>
    /// State machine for the UI element OptionView
    /// </summary>
    public class OptionViewStateMachine : StateMachine<OptionView>
    {
        private readonly State<OptionView> _idle;
        public readonly State<OptionView> _showing;
        public readonly State<OptionView> _hiding;

        public OptionViewStateMachine(OptionView managedObject) : base(managedObject)
        {
            _idle = new Idle(this);
            _showing = new Showing(this);
            _hiding = new Hiding(this);
            CurrentState = _idle;
        }
        
        public void SetState(State<OptionView> state)
        {
            CurrentState.Exit(state);
        }
            
        public void Update()
        {
            CurrentState.Execute();
        }
        
        private class Idle : State<OptionView>
        {
            public Idle(StateMachine<OptionView> stateMachine) : base(stateMachine) { }
        }
        private class Showing : State<OptionView>
        {
            private float _timer;

            protected override void Enter(StateMachine<OptionView> machine)
            {
                base.Enter(machine);
                StateMachine.ManagedObject.SetAsInteractable(false);
                StateMachine.ManagedObject.transform.localScale = Vector3.zero;
                StateMachine.ManagedObject.gameObject.SetActive(true);
                _timer = 0.0f;
            }

            public override void Execute()
            {
                _timer += Time.deltaTime;
                StateMachine.ManagedObject.transform.localScale = Vector3.Lerp(
                    StateMachine.ManagedObject.transform.localScale,
                    Vector3.one,
                    _timer/StateMachine.ManagedObject.AnimationDuration
                );
                if(_timer >= StateMachine.ManagedObject.AnimationDuration)
                {
                    StateMachine.ManagedObject.SetAsInteractable(true);
                    StateMachine.ManagedObject.transform.localScale = Vector3.one;
                    StateMachine.ManagedObject.SetAsInteractable(true);
                    this.Exit(((OptionViewStateMachine)StateMachine)._idle);
                }
            }

            public Showing(StateMachine<OptionView> stateMachine) : base(stateMachine) { }
        }

        private class Hiding : State<OptionView>
        {
            private float _timer;

            protected override void Enter(StateMachine<OptionView> machine)
            {
                base.Enter(machine);
                StateMachine.ManagedObject.SetAsInteractable(false);
                _timer = 0.0f;
            }

            public override void Execute()
            {
                _timer += Time.deltaTime;
                StateMachine.ManagedObject.transform.localScale = Vector3.Lerp(
                    StateMachine.ManagedObject.transform.localScale,
                    Vector3.zero,
                    _timer/StateMachine.ManagedObject.AnimationDuration
                );
                if(_timer >= StateMachine.ManagedObject.AnimationDuration)
                {
                    StateMachine.ManagedObject.transform.localScale = Vector3.zero;
                    this.Exit(((OptionViewStateMachine)StateMachine)._idle);
                }
            }

            public Hiding(StateMachine<OptionView> stateMachine) : base(stateMachine) { }
        }
    }    
}
