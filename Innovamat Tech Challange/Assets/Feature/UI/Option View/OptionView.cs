﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Feature.Core;
using Framework;
using UnityEngine.Serialization;

namespace Feature.UI
{
    /// <summary>
    /// UI element to display the challenge options
    /// </summary>
    public class OptionView : PoolableMonoBehaviour
    {
        private OptionViewStateMachine _stateMachine;
        private Challenge.Option _currentOption;
        public Challenge.Option CurrentOption => this._currentOption;
        public float AnimationDuration = 2.0f;
        [SerializeField]
        private Button _button;

        [SerializeField]
        private Text _optionDisplay;

        [SerializeField]
        private Image _background;

        private void Update()
        {
            _stateMachine.Update();
        }

        public void Dispose()
        {
            this._background.color = Color.white;
            this.OnDispose.Invoke(this);
        }

        public void Present(Challenge.Option option)
        {
            _stateMachine = new OptionViewStateMachine(this);
            Unsubscrive(_currentOption);
            _currentOption = option;
            Subscrive(_currentOption);
            _optionDisplay.text = this._currentOption.Value.ToString();
            _background.color = Color.white;
        }

        private void Unsubscrive(Challenge.Option option)
        {
            if (option == null) return;
            option.Challenge.OnOptionSumited -= RefreshState;
            option.Challenge.OnChallengeCompleted -= SetFinalState;
            this._button.onClick.RemoveListener(option.NotifyPick);
        }

        private void Subscrive(Challenge.Option option)
        {
            if (option == null) return;
            option.Challenge.OnOptionSumited += RefreshState;
            option.Challenge.OnChallengeCompleted += SetFinalState;
            this._button.onClick.AddListener(option.NotifyPick);
        }

        private void RefreshState(Challenge.OptionSumission sumission)
        {
            if (sumission.ChallangeMustEnd)
            {
                SetAsCorrect(sumission.IsCorrect && sumission.Option.Equals(_currentOption));
                SetAsShowing(false);
            }
            else
            {
                if (!sumission.Option.Equals(_currentOption)) return;
                SetAsCorrect(false);
                SetAsShowing(false);
            }
        }

        private void SetFinalState(Challenge.OptionSumission sumission)
        {
            SetAsCorrect(_currentOption.IsCorrect);
        }

        private void SetAsCorrect(bool isCorrect) => this._background.color = (isCorrect) ? Color.green : Color.red;

        public void SetAsInteractable(bool isInteractable) => this._button.interactable = isInteractable;

        public void SetAsShowing(bool isShowing) =>
            _stateMachine.SetState((isShowing) ? _stateMachine._showing : _stateMachine._hiding);

    }
}
