﻿namespace Framework
{
    /// <summary>
    /// State Machine generalitaztion
    /// </summary>
    public abstract class StateMachine<T>
    {
        public readonly T ManagedObject;
        public State<T> CurrentState;
        protected StateMachine(T managedObject)
        {
            ManagedObject = managedObject;
        }
    }
}