﻿namespace Framework
{
    /// <summary>
    /// State of state machine generalitzation
    /// </summary>
    public class State<T>
    {
        protected StateMachine<T> StateMachine;
        
        public State(StateMachine<T> stateMachine)
        {
            StateMachine = stateMachine;
        }

        protected virtual void Enter(StateMachine<T> stateMachine)
        {
            StateMachine.CurrentState = this;
        }

        public virtual void Execute() { }

        public void Exit(State<T> to)
        {
            StateMachine = to.StateMachine;
            to.Enter(StateMachine);
        }
    }
}
