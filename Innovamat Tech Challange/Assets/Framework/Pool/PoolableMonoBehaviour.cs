﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Framework
{
    /// <summary>
    /// Poolable objects generalitzation to be used with the Pool
    /// </summary>
    public abstract class PoolableMonoBehaviour : MonoBehaviour
    {
        public Action<PoolableMonoBehaviour> OnDispose;

        public void Dispose() => OnDispose.Invoke(this);
    }
}