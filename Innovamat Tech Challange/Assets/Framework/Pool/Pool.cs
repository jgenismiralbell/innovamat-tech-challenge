﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Framework
{
    /// <summary>
    /// Pool system for all the PoolableMonoBehaviour
    /// </summary>
    public class Pool<T> where T : PoolableMonoBehaviour
    {
        private readonly T _orginal;
        private readonly List<T> _inUse = new List<T>();
        private readonly List<T> _reserve = new List<T>();

        public Pool(T original)
        {
            _orginal = original;
        }

        public T Get(Transform at)
        {
            T instace = null;
            if(this._reserve.Count == 0)
            {
                instace = GameObject.Instantiate(this._orginal, at);
                instace.OnDispose += this.Dispose;
                this._inUse.Add(instace);
            }
            else
            {
                instace = this._reserve[0];
                this._reserve.Remove(instace);
                this._inUse.Add(instace);
            }
            return instace;
        }

        private void Dispose(PoolableMonoBehaviour instance)
        {
            if(!(instance is T pooleable)) return;
            instance.gameObject.SetActive(false);
            this._inUse.Remove(pooleable);
            this._reserve.Add(pooleable);
        }
    }
}