﻿using UnityEngine;
using System;

namespace Framework
{
    /// <summary>
    /// Parser for TextAsset
    /// </summary>
    public static class TextFileParser
    {
        public static string[][] Parse(this TextAsset asset, ParseFormat format, bool excludeHeader = true)
        {
            if(!excludeHeader)
            {
                string[] lines = asset.text.Split(format.LineSeparator(), StringSplitOptions.RemoveEmptyEntries);

                string[][] value = new string[lines.Length][];

                for (int i = 0; i < lines.Length; i++)
                {
                    value[i] = lines[i].Split(format.FieldSeparator());
                }
                return value;
            }
            else
            {
                string[] lines = asset.text.Split(format.LineSeparator(), StringSplitOptions.RemoveEmptyEntries);

                string[][] value = new string[lines.Length - 1][];

                for (int i = 1; i < lines.Length; i++)
                {
                    value[i-1] = lines[i].Split(format.FieldSeparator());
                }
                return value;
            }
        }
        public static char[] LineSeparator(this ParseFormat format)
        {
            char[] lineSeparator = new char[1];
            switch(format)
            {
                case ParseFormat.CSV:
                    return Environment.NewLine.ToCharArray();
                case ParseFormat.TSV:
                    return Environment.NewLine.ToCharArray();
                default:
                    return lineSeparator;
            }
        }

        public static char[] FieldSeparator(this ParseFormat format)
        {
            char[] fieldSeparator = new char[1];
            switch(format)
            {
                case ParseFormat.CSV:
                    fieldSeparator[0] = ',';
                    return fieldSeparator;
                case ParseFormat.TSV:
                    fieldSeparator[0] = '\t';
                    return fieldSeparator;
                default:
                    return fieldSeparator;
            }
        }
    }

    public enum ParseFormat
    {
        CSV,
        TSV
    }

}
